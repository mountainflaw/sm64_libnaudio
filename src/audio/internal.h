#pragma once

#include <ultra64.h>
#include "types.h"

#define         NBUFFERS       64

typedef struct {
    ALLink      node;
    int         startAddr;
    u32         lastFrame;
    char        *ptr;
} DMABuffer;

typedef struct {
    u8          initialized;
    DMABuffer   *firstUsed;
    DMABuffer   *firstFree;

} DMAState;

#define MAX_BUFFER_LENGTH 1024
#define DMA_QUEUE_SIZE    50
#define MAX_CLIST_SIZE    5000
#define MAX_AUDIO_LENGTH  4096
#define NUM_FIELDS        1
#define MAX_SEQ_LENGTH    50000

#define MAX_UPDATES     128
#define EVT_COUNT       128
#define MAX_VOICES      32
#define OUTPUT_RATE     32000

extern ALDMAproc dma_new(DMAState **state);

extern ALHeap laHeap;
extern ALSynConfig laCfg;
extern ALGlobals laGlobals;

extern u32 gGlobalAudioTimer;

extern struct SPTask gAudioTasks[2];
extern struct SPTask *currentTask;
extern u16 gCurrentAudioTaskIndex;
extern Acmd *gAudioCmdList[2];

extern OSMesgQueue dmaMessageQ, taskMessageQ, retraceMessageQ;

extern Acmd *gAudioListHead;

extern s16 *gAudioBuffer[3];
extern u8 gCurrentAudioBuffer;
extern s16 gAudioSamples[3];

extern s32 gAudioCommandLength;