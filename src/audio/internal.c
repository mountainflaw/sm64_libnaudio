// Internal globals and whatnot are defined here.

#include <ultra64.h>

#include "types.h"

ALHeap laHeap;
ALSynConfig laCfg;
ALGlobals laGlobals;

u32 gGlobalAudioTimer = 0;

s16 *gAudioBuffer[3];
u8 gCurrentAudioBuffer = 0;
s16 gAudioSamples[3];

s32 gAudioCommandLength = 0;

struct SPTask gAudioTasks[2];
struct SPTask *currentTask;
u16 gCurrentAudioTaskIndex = 0;

Acmd *gAudioCmdList[2];
Acmd *gAudioListHead;