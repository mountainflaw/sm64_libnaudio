#include <ultra64.h>

#include "game/memory.h"
#include "buffers/buffers.h"

#include "types.h"

#include "internal.h"
#include "external.h"

#include "junk.inc.h" // sm64 neos stubbed junk

void audio_init() {
    osSyncPrintf("RGFX AUDIO: init\n");

    osSyncPrintf("RGFX AUDIO: REACHED task init\n");

    laCfg.maxVVoices = MAX_VOICES;
    laCfg.maxPVoices = MAX_VOICES;
    laCfg.maxUpdates = MAX_UPDATES;
    laCfg.outputRate = osAiSetFrequency(OUTPUT_RATE);

    osSyncPrintf("RGFX AUDIO: REACHED heap init\n");
        laCfg.heap = &laHeap;
    alHeapInit(&laHeap, &gAudioHeap, AUDIO_HEAP_SIZE);

    gAudioCmdList[0] = alHeapAlloc(&laHeap, 1, MAX_CLIST_SIZE * sizeof(Acmd));
    gAudioCmdList[1] = alHeapAlloc(&laHeap, 1, MAX_CLIST_SIZE * sizeof(Acmd));

    gAudioBuffer[0] = alHeapAlloc(&laHeap, 1, sizeof(s32)*MAX_AUDIO_LENGTH);
    gAudioBuffer[1] = alHeapAlloc(&laHeap, 1, sizeof(s32)*MAX_AUDIO_LENGTH);
    gAudioBuffer[2] = alHeapAlloc(&laHeap, 1, sizeof(s32)*MAX_AUDIO_LENGTH);

    osSyncPrintf("RGFX AUDIO: REACHED dma init\n");
    laCfg.dmaproc = &dma_new;

    osSyncPrintf("RGFX AUDIO: REACHED alInit\n");
    laCfg.fxType = AL_FX_SMALLROOM;
    alInit(&laGlobals, &laCfg);
} // in load.c

struct SPTask *audio_frame() {
    osSyncPrintf("RGFX AUDIO: Audio frame\n");
    
    if (gAudioCommandLength == 0) { // no sound
        return NULL;
    }

    currentTask = &gAudioTasks[gCurrentAudioTaskIndex];

    gAudioListHead = gAudioCmdList[gCurrentAudioTaskIndex];

    gAudioListHead = alAudioFrame(gAudioListHead, &gAudioCommandLength, VIRTUAL_TO_PHYSICAL(gAudioBuffer[gCurrentAudioBuffer]), gAudioSamples[gCurrentAudioBuffer]);

    currentTask->msg = NULL;
    currentTask->msgqueue = NULL;

    currentTask->task.t.type = M_AUDTASK;
    currentTask->task.t.flags = 0;
    currentTask->task.t.ucode_boot = rspbootTextStart;
    currentTask->task.t.ucode_boot_size = (u8 *) rspbootTextEnd - (u8 *) rspbootTextStart;
    currentTask->task.t.ucode = aspMainTextStart;
    currentTask->task.t.ucode_size = 0x800; // (this size is ignored)
    currentTask->task.t.ucode_data = aspMainDataStart;
    currentTask->task.t.ucode_data_size = (aspMainDataEnd - aspMainDataStart) * sizeof(u64);
    currentTask->task.t.dram_stack = NULL;
    currentTask->task.t.dram_stack_size = 0;
    currentTask->task.t.output_buff = NULL;
    currentTask->task.t.output_buff_size = NULL;
    currentTask->task.t.data_ptr = gAudioCmdList[gCurrentAudioTaskIndex];
    currentTask->task.t.data_size = gAudioCommandLength * sizeof(u64);
    currentTask->task.t.yield_data_ptr = NULL;
    currentTask->task.t.yield_data_size = 0;


    //currentTask->task.t.data_size = 4 * sizeof(Acmd);
    // stats

    osSyncPrintf("-- STATS ABOUT THIS AUDIO FRAME --\n");
    osSyncPrintf("Frame: %d\n", gGlobalAudioTimer);
    osSyncPrintf("Buffer index: %d\n", gCurrentAudioBuffer);
    osSyncPrintf("Task index: %d\n\n", gCurrentAudioTaskIndex);

    osSyncPrintf("Type: %d\n", currentTask->task.t.type);
    osSyncPrintf("Ucode addr text: %x\n", currentTask->task.t.ucode);
    osSyncPrintf("Ucode addr data: %x\n\n", currentTask->task.t.ucode_data);

    osSyncPrintf("Ucode text size : %x\n", currentTask->task.t.ucode_size);
    osSyncPrintf("Ucode data size : %x\n\n", currentTask->task.t.ucode_data_size);

    osSyncPrintf("Ucode command addr: %x\n", currentTask->task.t.data_ptr);
    osSyncPrintf("Ucode command size: %x\n\n", currentTask->task.t.data_size);
    
// end of frame

    gCurrentAudioBuffer++;
    gCurrentAudioTaskIndex++;
    
    if (gCurrentAudioBuffer > 2) {
        gCurrentAudioBuffer = 0;
    }

    if (gCurrentAudioTaskIndex > 1) { // this is technically inefficient, but allows for triple buffering later on
        gCurrentAudioTaskIndex = 0;
    }
    gGlobalAudioTimer++;

    //return NULL;
    return currentTask;
}